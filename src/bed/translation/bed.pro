SOURCES += \
../__init__.py \
../__pycache__ \
../document.py \
../editor.py \
../element.py \
../frame.py \
../group.py \
../helpers.py \
../highlighter.py \
../icons \
../latex \
../launcher \
../painter.py \
../parsing.py \
../settings.py \
../translation \
../undo.py \

TRANSLATIONS += fr.ts
