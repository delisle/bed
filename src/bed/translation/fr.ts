<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR" sourcelanguage="en_US">
<context>
    <name>Arrow</name>
    <message>
        <location filename="../element.py" line="2378"/>
        <source>Pages</source>
        <translation>Pages</translation>
    </message>
    <message>
        <location filename="../element.py" line="2377"/>
        <source>Opt. args.</source>
        <translation>Args. opt.</translation>
    </message>
    <message>
        <location filename="../element.py" line="2379"/>
        <source>x guides</source>
        <translation>Guides x</translation>
    </message>
    <message>
        <location filename="../element.py" line="2380"/>
        <source>y guides</source>
        <translation>Guides y</translation>
    </message>
    <message>
        <location filename="../element.py" line="2375"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../element.py" line="2395"/>
        <source>Arrow</source>
        <translation>Flèche</translation>
    </message>
</context>
<context>
    <name>BEd</name>
    <message>
        <location filename="../__init__.py" line="109"/>
        <source>&amp;New document</source>
        <translation>&amp;Nouveau document</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="113"/>
        <source>Start a new document</source>
        <comment>BEd</comment>
        <translation>Commencer un nouveau document</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="121"/>
        <source>&amp;Open</source>
        <translation>&amp;Ouvrir</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="157"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="162"/>
        <source>Save TeX file, compile, and reload</source>
        <translation>Enregistrer le fichier TeX, compiler et recharger</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="177"/>
        <source>Save TeX file as..., compile, and reload</source>
        <translation>Enregistrer le fichier TeX sous..., compiler et recharger</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="187"/>
        <source>&amp;Exit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="191"/>
        <source>Exit application</source>
        <translation>Quitter l&apos;application</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="202"/>
        <source>Go to the previous page</source>
        <translation>Aller à la page précédente</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="211"/>
        <source>Go to the next page</source>
        <translation>Aller à la page suivante</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="329"/>
        <source>&amp;Group</source>
        <translation>&amp;Grouper</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="333"/>
        <source>Group selected objects</source>
        <translation>Grouper les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="341"/>
        <source>&amp;Ungroup</source>
        <translation>&amp;Dégrouper</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="345"/>
        <source>Ungroup selected objects</source>
        <translation>Dégrouper les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="610"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="247"/>
        <source>Edit selected objects</source>
        <translation>Éditer les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="265"/>
        <source>&amp;Copy</source>
        <translation>Co&amp;pier</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="269"/>
        <source>Copy selected objects</source>
        <translation>Copier les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="285"/>
        <source>&amp;Paste</source>
        <translation>&amp;Coller</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="289"/>
        <source>Paste selected objects</source>
        <translation>Coller les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="297"/>
        <source>&amp;Delete</source>
        <translation>&amp;Supprimer</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="301"/>
        <source>Delete selected objects</source>
        <translation>Supprimer les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="307"/>
        <source>&amp;Undo</source>
        <translation>Annu&amp;ler</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="311"/>
        <source>Undo last action</source>
        <translation>Annuler la dernière action</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="317"/>
        <source>&amp;Redo</source>
        <translation>&amp;Rétablir</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="321"/>
        <source>Redo action</source>
        <translation>Rétablir l&apos;action</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="355"/>
        <source>Add a new frame</source>
        <translation>Ajouter un nouveau transparent</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="416"/>
        <source>Add a new text block</source>
        <translation>Ajouter un nouveau bloc de texte</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="428"/>
        <source>Add a new image</source>
        <translation>Ajouter une nouvelle image</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="452"/>
        <source>Add a new arrow</source>
        <translation>Ajouter une nouvelle flèche</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="462"/>
        <source>Move up selection a notch</source>
        <translation>Déplacer la sélection d&apos;un cran vers le haut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="472"/>
        <source>Move down selection a notch</source>
        <translation>Déplacer la sélection d&apos;un cran vers le bas</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="482"/>
        <source>Move selection to top level</source>
        <translation>Déplacer la sélection tout en haut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="492"/>
        <source>Move selection to bottom level</source>
        <translation>Déplacer la sélection tout en bas</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="507"/>
        <source>(De)activate grid guides</source>
        <translation>(Dés)activer les guides en grille</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="523"/>
        <source>(De)activate object guides</source>
        <translation>(Dés)activer les guides d&apos;objet</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="574"/>
        <source>Save current settings</source>
        <translation>Enregistrer les réglages actuels</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="582"/>
        <source>&amp;Auto save settings</source>
        <translation>&amp;Auto-enregistrer les réglages</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="591"/>
        <source>Automatically save settings on exit</source>
        <translation>Enregistrer automatiquement les réglages à la fermeture</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="601"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="624"/>
        <source>&amp;Insert</source>
        <translation>&amp;Insertion</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="635"/>
        <source>&amp;Depth</source>
        <translation>&amp;Profondeur</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="641"/>
        <source>&amp;Configuration</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="661"/>
        <source>Frame</source>
        <translation>Transparent</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="663"/>
        <source>Page</source>
        <translation>Page</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="665"/>
        <source>Page in Frame</source>
        <translation>Page du transparent</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="677"/>
        <source>Pagebar</source>
        <translation>barre de page</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="757"/>
        <source>Open File</source>
        <translation>Ouvrir un fichier</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="908"/>
        <source>The document could not be compiled and no backup of the TeX file was found. Fix the issue and retry to compile.

{err}</source>
        <translation>Le document n&apos;a pas pu être compilé et aucune sauvegarde du fichier TeX n&apos;a été trouvée. Réparez le problème et réessayer de compiler.

{err}</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1041"/>
        <source>Save File</source>
        <translation>Enregistrer le fichier</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1076"/>
        <source>Unsaved changes</source>
        <translation>Changements non-enregistrés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1077"/>
        <source>Current document has been modified. Save modifications?</source>
        <translation>Le document ouvert a été modifié. Enregistrer les modifications ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="172"/>
        <source>Save &amp;as...</source>
        <translation>Enregistrer &amp;sous...</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="199"/>
        <source>Previous page</source>
        <translation>Page précédente</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="208"/>
        <source>Next page</source>
        <translation>Page suivante</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="255"/>
        <source>Edit &amp;document</source>
        <translation>Éditer le &amp;document</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="351"/>
        <source>New &amp;frame</source>
        <translation>Nouveau &amp;transparent</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="412"/>
        <source>New &amp;text</source>
        <translation>Nouveau te&amp;xte</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="424"/>
        <source>New &amp;image</source>
        <translation>Nouvelle &amp;image</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="448"/>
        <source>New &amp;arrow</source>
        <translation>Nouvelle &amp;flèche</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="458"/>
        <source>Move &amp;up</source>
        <translation>Vers le &amp;haut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="468"/>
        <source>Move &amp;down</source>
        <translation>Vers le &amp;bas</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="488"/>
        <source>Move &amp;bottom</source>
        <translation>Tout en &amp;bas</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="478"/>
        <source>Move &amp;top</source>
        <translation>Tout en &amp;haut</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="544"/>
        <source>Show/Hide &amp;menu</source>
        <translation>Montrer/Cacher le &amp;menu</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="549"/>
        <source>Show/Hide menu bar</source>
        <translation>Montrer/Cacher la barre de menu</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="570"/>
        <source>&amp;Save settings</source>
        <translation>&amp;Enregistrer les réglages</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="243"/>
        <source>&amp;Edit...</source>
        <translation>&amp;Éditer...</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="259"/>
        <source>Edit document parameters (LaTeX header...)</source>
        <translation>Éditer les paramètres du document (entête LaTeX...)</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="219"/>
        <source>Select &amp;all...</source>
        <translation>&amp;Tout sélectionner...</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="223"/>
        <source>Select all objects in current frame</source>
        <translation>Sélectionner tous les objets dans le transparent actuel</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="231"/>
        <source>Unselect &amp;all...</source>
        <translation>&amp;Tout désélectionner...</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="235"/>
        <source>Unselect all objects in current frame</source>
        <translation>Désélectionner tous les objets dans le transparent actuel</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="275"/>
        <source>&amp;Cut</source>
        <translation>&amp;Couper</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="279"/>
        <source>Cut selected objects</source>
        <translation>Couper les objets sélectionnés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="500"/>
        <source>&amp;Grid</source>
        <translation>&amp;Grille</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="516"/>
        <source>&amp;Object guides</source>
        <translation>&amp;Guides d&apos;objet</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="811"/>
        <location filename="../__init__.py" line="871"/>
        <location filename="../__init__.py" line="1003"/>
        <source>LaTeX error detected</source>
        <translation>Erreur LaTeX détectée</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="887"/>
        <source>The document could not be compiled. The previous saved version of the document has been copied in the current directory with a .bak extension. Do you want to reset the document to this backup (you will lose all changes made) or continue to edit the current non-working version to try and fix it?</source>
        <translation>Le document n&apos;a pas pu être compilé. La version sauvegardée précédente a été copiée dans le dossier courant avec une extension .bak. Voulez-vous réinitialiser le document à partir de cette sauvegarde (vous perdrez tous les changements) ou continuer d&apos;éditer la version cassée courante et essayer de la réparer ?</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="919"/>
        <source>The document could not be open in BEd (LaTeX compiling issue). You will have to fix the issue in an external editor and retry and open the document in BEd.</source>
        <translation>Le document n&apos;a pas pu être ouvert dans BEd (problème de compilation LaTeX). Vous allez devoir réparer le problème dans un éditeur externe et essayer d&apos;ouvrir à nouveau le document dans BEd.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="125"/>
        <source>Open TeX file</source>
        <translation>Ouvrir un fichier TeX</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="133"/>
        <source>&amp;Reload</source>
        <translation>&amp;Recharger</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="137"/>
        <source>Reload TeX file</source>
        <translation>Recharger le fichier TeX</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="164"/>
        <source>Save TeX file</source>
        <translation>Enregistrer le fichier TeX</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="179"/>
        <source>Save TeX file as...</source>
        <translation>Enregistrer le fichier TeX sous...</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="145"/>
        <source>&amp;Preview</source>
        <translation>&amp;Prévisualisation</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="149"/>
        <source>Preview current frame</source>
        <translation>Prévisualiser le transparent courant</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="558"/>
        <source>&amp;Edit settings</source>
        <translation>&amp;Éditer les réglages</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="562"/>
        <source>Edit settings</source>
        <translation>Éditer les réglages</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="367"/>
        <location filename="../__init__.py" line="1359"/>
        <source>Template</source>
        <translation>Modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="376"/>
        <source>Add a new frame from the template</source>
        <translation>Ajouter un nouveau transparent à partir du modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="388"/>
        <source>Save &amp;template</source>
        <translation>Enregistrer un &amp;modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="392"/>
        <source>Save current frame as template</source>
        <translation>Enregistrer le transparent actuel comme modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1357"/>
        <source>New template</source>
        <translation>Nouveau modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1358"/>
        <location filename="../__init__.py" line="1376"/>
        <source>Template name</source>
        <translation>Nom du modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="398"/>
        <source>&amp;Delete template</source>
        <translation>&amp;Supprimer un modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="404"/>
        <source>Delete one of the saved templates</source>
        <translation>Supprimer un des modèles enregistrés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1375"/>
        <source>Delete template</source>
        <translation>Supprimer un modèle</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="436"/>
        <source>New &amp;Tikz picture</source>
        <translation>Nouveau dessin &amp;Tikz</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="442"/>
        <source>Add a new Tikz picture</source>
        <translation>Ajouter un nouveau dessin Tikz</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="817"/>
        <source>BEd can only load LaTeX files that include the bed LaTeX package. Please add &quot;\usepackage{bed}&quot; in the preamble of the file and retry.</source>
        <translation>BEd ne peut charger que les fichiers LaTeX qui inclus le package LaTeX bed. Ajoutez &amp;quot;\usepackage{bed}&amp;quot; au préambule du fichier et réessayez.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="530"/>
        <source>&amp;Hidden objects</source>
        <translation>&amp;Objets cachés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="537"/>
        <source>Reveal hidden objects</source>
        <translation>Révéler les objects cachés</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1011"/>
        <source>The current frame could not be compiled. Fix the issue and retry to compile.</source>
        <translation>Le transparent courant n&apos;a pas pu être compilé. Réparez le problème et rééssayer de compiler.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1958"/>
        <source>BEd error</source>
        <translation>Erreur BEd</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1960"/>
        <source>The following error was detected:</source>
        <translation>L&apos;erreur suivante a été détectée :</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1963"/>
        <source>Try to save your document and restart BEd.</source>
        <translation>Essayez de sauvegarder votre document et de redémarrer BEd.</translation>
    </message>
    <message>
        <location filename="../__init__.py" line="1964"/>
        <source>Please consider filling a bug report at:</source>
        <translation>Merci de remplir un rapport de bug à :</translation>
    </message>
</context>
<context>
    <name>BedWidget</name>
    <message>
        <location filename="../editor.py" line="114"/>
        <source>Reset</source>
        <translation>Réinitialiser</translation>
    </message>
</context>
<context>
    <name>Document</name>
    <message>
        <location filename="../document.py" line="144"/>
        <source>Header</source>
        <translation>Entête</translation>
    </message>
    <message>
        <location filename="../document.py" line="147"/>
        <source>Footer</source>
        <translation>Pied de page</translation>
    </message>
    <message>
        <location filename="../document.py" line="152"/>
        <source>Document</source>
        <translation>Document</translation>
    </message>
</context>
<context>
    <name>Frame</name>
    <message>
        <location filename="../frame.py" line="362"/>
        <source>Title &amp; header</source>
        <translation>Titre &amp; entête</translation>
    </message>
    <message>
        <location filename="../frame.py" line="359"/>
        <source>Before frame</source>
        <translation>Avant le transparent</translation>
    </message>
    <message>
        <location filename="../frame.py" line="367"/>
        <source>Frame</source>
        <translation>Transparent</translation>
    </message>
</context>
<context>
    <name>Group</name>
    <message>
        <location filename="../group.py" line="338"/>
        <source>Lock ratio</source>
        <translation>Bloquer le rapport</translation>
    </message>
    <message>
        <location filename="../group.py" line="340"/>
        <source>Original ratio</source>
        <translation>Rapport de l&apos;original</translation>
    </message>
    <message>
        <location filename="../group.py" line="344"/>
        <source>x guides</source>
        <translation>Guides x</translation>
    </message>
    <message>
        <location filename="../group.py" line="345"/>
        <source>y guides</source>
        <translation>Guides y</translation>
    </message>
    <message>
        <location filename="../group.py" line="342"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../group.py" line="343"/>
        <source>Pages</source>
        <translation>Pages</translation>
    </message>
    <message>
        <location filename="../group.py" line="360"/>
        <source>Group</source>
        <translation>Groupe</translation>
    </message>
</context>
<context>
    <name>Image</name>
    <message>
        <location filename="../element.py" line="1759"/>
        <source>Pages</source>
        <translation>Pages</translation>
    </message>
    <message>
        <location filename="../element.py" line="1698"/>
        <source>Open Image</source>
        <translation>Ouvrir une image</translation>
    </message>
    <message>
        <location filename="../element.py" line="1736"/>
        <source>Lock ratio</source>
        <translation>Bloquer le rapport</translation>
    </message>
    <message>
        <location filename="../element.py" line="1738"/>
        <source>Original ratio</source>
        <translation>Rapport de l&apos;original</translation>
    </message>
    <message>
        <location filename="../element.py" line="1753"/>
        <source>Filename</source>
        <translation>Nom de fichier</translation>
    </message>
    <message>
        <location filename="../element.py" line="1755"/>
        <source>Browse</source>
        <translation>Parcourir</translation>
    </message>
    <message>
        <location filename="../element.py" line="1758"/>
        <source>Opt. args.</source>
        <translation>Args. opt.</translation>
    </message>
    <message>
        <location filename="../element.py" line="1760"/>
        <source>x guides</source>
        <translation>Guides x</translation>
    </message>
    <message>
        <location filename="../element.py" line="1761"/>
        <source>y guides</source>
        <translation>Guides y</translation>
    </message>
    <message>
        <location filename="../element.py" line="1742"/>
        <source>Crop left</source>
        <translation>Découp. gauche</translation>
    </message>
    <message>
        <location filename="../element.py" line="1745"/>
        <source>Crop bottom</source>
        <translation>Découp. bas</translation>
    </message>
    <message>
        <location filename="../element.py" line="1748"/>
        <source>Crop right</source>
        <translation>Découp. droite</translation>
    </message>
    <message>
        <location filename="../element.py" line="1751"/>
        <source>Crop top</source>
        <translation>Découp. haut</translation>
    </message>
    <message>
        <location filename="../element.py" line="1740"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../element.py" line="1784"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../element.py" line="1757"/>
        <source>Link</source>
        <translation>Lien</translation>
    </message>
</context>
<context>
    <name>PainterArea</name>
    <message>
        <location filename="../painter.py" line="143"/>
        <source>Compiling...</source>
        <translation>Compilation...</translation>
    </message>
</context>
<context>
    <name>PropertiesEditor</name>
    <message>
        <location filename="../editor.py" line="365"/>
        <source>BEd - Properties Editor</source>
        <translation>BEd - Editeur de propriétés</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../settings.py" line="171"/>
        <source>Settings</source>
        <translation>Réglages</translation>
    </message>
    <message>
        <location filename="../settings.py" line="523"/>
        <source>Text frame</source>
        <translation>Transparent de texte</translation>
    </message>
    <message>
        <location filename="../settings.py" line="528"/>
        <source>Background text frame</source>
        <translation>Transparent de texte en fond</translation>
    </message>
    <message>
        <location filename="../settings.py" line="532"/>
        <source>Title frame</source>
        <translation>Transparent de titre</translation>
    </message>
    <message>
        <location filename="../settings.py" line="534"/>
        <source>Outline frame</source>
        <translation>Transparent de plan</translation>
    </message>
    <message>
        <location filename="../settings.py" line="191"/>
        <source>LaTeX command</source>
        <translation>Commande LaTeX</translation>
    </message>
    <message>
        <location filename="../settings.py" line="271"/>
        <source>Text [Default y]</source>
        <translation>Texte [y par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="436"/>
        <source>Style [Element]</source>
        <translation>Style [Élément]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="197"/>
        <source>Display resolution (dpi)</source>
        <translation>Résolution de l&apos;affichage (ppp)</translation>
    </message>
    <message>
        <location filename="../settings.py" line="202"/>
        <source>Automatically reload document when saving</source>
        <translation>Recharger automatiquement le document en sauvegardant</translation>
    </message>
    <message>
        <location filename="../settings.py" line="207"/>
        <source>Number of previews kept in undo stack</source>
        <translation>Nombre de prévisualisations conservées (pile annuler/rétablir)</translation>
    </message>
    <message>
        <location filename="../settings.py" line="210"/>
        <source>Number of lines in grid</source>
        <translation>Nombre de lignes dans la grille</translation>
    </message>
    <message>
        <location filename="../settings.py" line="214"/>
        <source>Normal move step size</source>
        <translation>Taille du pas de déplacement normal</translation>
    </message>
    <message>
        <location filename="../settings.py" line="218"/>
        <source>Small move step size</source>
        <translation>Taille du pas de petit déplacement</translation>
    </message>
    <message>
        <location filename="../settings.py" line="222"/>
        <source>Large move step size</source>
        <translation>Taille du pas de grand déplacement</translation>
    </message>
    <message>
        <location filename="../settings.py" line="226"/>
        <source>Rounding precision</source>
        <translation>Précision des arrondis</translation>
    </message>
    <message>
        <location filename="../settings.py" line="231"/>
        <source>Default LaTeX header</source>
        <translation>Entête LaTeX par défaut</translation>
    </message>
    <message>
        <location filename="../settings.py" line="254"/>
        <source>Default LaTeX footer</source>
        <translation>Pied de page LaTeX par défaut</translation>
    </message>
    <message>
        <location filename="../settings.py" line="260"/>
        <source>Default title page content</source>
        <translation>Contenu de la page de titre par défaut</translation>
    </message>
    <message>
        <location filename="../settings.py" line="264"/>
        <source>Default frame title</source>
        <translation>Titre de transparent par défaut</translation>
    </message>
    <message>
        <location filename="../settings.py" line="268"/>
        <source>Text [Default x]</source>
        <translation>Texte [x par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="274"/>
        <source>Text [Default width]</source>
        <translation>Texte [Largeur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="277"/>
        <source>Text [Default height]</source>
        <translation>Texte [Hauteur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="280"/>
        <source>Text [Default text]</source>
        <translation>Texte [Texte par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="284"/>
        <source>Image [Default x]</source>
        <translation>Image [x par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="287"/>
        <source>Image [Default y]</source>
        <translation>Image [y par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="290"/>
        <source>Image [Default width]</source>
        <translation>Image [Largeur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="293"/>
        <source>Image [Default height]</source>
        <translation>Image [Hauteur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="296"/>
        <source>Tikz picture [Default x]</source>
        <translation>Dessin Tikz [x par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="299"/>
        <source>Tikz picture [Default y]</source>
        <translation>Dessin Tikz [y par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="304"/>
        <source>Tikz picture [Default width]</source>
        <translation>Dessin Tikz [Largeur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="309"/>
        <source>Tikz picture [Default height]</source>
        <translation>Dessin Tikz [Hauteur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="314"/>
        <source>Tikz picture [Default command]</source>
        <translation>Dessin Tikz [Commande par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="324"/>
        <source>Arrow [Default x]</source>
        <translation>Flèche [x par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="327"/>
        <source>Arrow [Default y]</source>
        <translation>Flèche [y par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="330"/>
        <source>Arrow [Default width]</source>
        <translation>Flèche [Largeur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="333"/>
        <source>Arrow [Default line width]</source>
        <translation>Flèche [Épaisseur par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="336"/>
        <source>Arrow [Default options]</source>
        <translation>Flèche [Options par défaut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="343"/>
        <source>Shortcut [Auto save settings]</source>
        <translation>Raccourcis [Auto-enregistrer les réglages]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="346"/>
        <source>Shortcut [Edit settings]</source>
        <translation>Raccourcis [Éditer les réglages]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="349"/>
        <source>Shortcut [Save settings]</source>
        <translation>Raccourcis [Sauvegarder les réglages]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="352"/>
        <source>Shortcut [Reload]</source>
        <translation>Raccourcis [Recharger]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="355"/>
        <source>Shortcut [Preview]</source>
        <translation>Raccourcis [Prévisualisation]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="358"/>
        <source>Shortcut [Edit...]</source>
        <translation>Raccourcis [Éditer...]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="361"/>
        <source>Shortcut [Edit document]</source>
        <translation>Raccourcis [Éditer le document]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="364"/>
        <source>Shortcut [Group]</source>
        <translation>Raccourcis [Grouper]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="367"/>
        <source>Shortcut [Ungroup]</source>
        <translation>Raccourcis [Dégrouper]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="370"/>
        <source>Shortcut [New frame]</source>
        <translation>Raccourcis [Nouveau transparent]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="375"/>
        <source>Shortcut [Template + number]</source>
        <translation>Raccourcis [Modèle + numéro]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="378"/>
        <source>Shortcut [Save template]</source>
        <translation>Raccourcis [Enregistrer un modèle]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="383"/>
        <source>Shortcut [Delete template]</source>
        <translation>Raccourcis [Supprimer un modèle]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="386"/>
        <source>Shortcut [New text]</source>
        <translation>Raccourcis [Nouveau texte]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="389"/>
        <source>Shortcut [New image]</source>
        <translation>Raccourcis [Nouvelle image]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="394"/>
        <source>Shortcut [New Tikz picture]</source>
        <translation>Raccourcis [Nouveau dessin Tikz]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="397"/>
        <source>Shortcut [New arrow]</source>
        <translation>Raccourcis [Nouvelle flèche]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="400"/>
        <source>Shortcut [Move up]</source>
        <translation>Raccourcis [Vers le haut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="403"/>
        <source>Shortcut [Move down]</source>
        <translation>Raccourcis [Vers le bas]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="406"/>
        <source>Shortcut [Move top]</source>
        <translation>Raccourcis [Tout en haut]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="409"/>
        <source>Shortcut [Move bottom]</source>
        <translation>Raccourcis [Tout en bas]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="412"/>
        <source>Shortcut [Grid]</source>
        <translation>Raccourcis [Grille]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="417"/>
        <source>Shortcut [Object guides]</source>
        <translation>Raccourcis [Guides d&apos;objet]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="422"/>
        <source>Shortcut [Hidden objects]</source>
        <translation>Raccourcis [Objets cachés]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="425"/>
        <source>Shortcut [Show/Hide menu]</source>
        <translation>Raccourcis [Montrer/Cacher le menu]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="430"/>
        <source>Color [Background]</source>
        <translation>Couleur [Fond]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="433"/>
        <source>Color [New element]</source>
        <translation>Couleur [Nouvel élément]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="439"/>
        <source>Style [Selected element]</source>
        <translation>Style [Élément sélectionné]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="442"/>
        <source>Style [Group]</source>
        <translation>Style [Groupe]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="445"/>
        <source>Style [Selected group]</source>
        <translation>Style [Groupe sélectionné]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="448"/>
        <source>Style [Hidden object]</source>
        <translation>Style [Objet caché]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="453"/>
        <source>Style [Selected hidden object]</source>
        <translation>Style [Objet caché sélectionné]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="458"/>
        <source>Style [grid level 1]</source>
        <translation>Style [Grille niveau 1]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="461"/>
        <source>Style [grid level 2]</source>
        <translation>Style [Grille niveau 2]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="464"/>
        <source>Style [grid level 3]</source>
        <translation>Style [Grille niveau 3]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="467"/>
        <source>Style [grid level 4]</source>
        <translation>Style [Grille niveau 4]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="470"/>
        <source>Style [object_guide]</source>
        <translation>Style [Guide d&apos;objet]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="475"/>
        <source>Style [Selected object guide]</source>
        <translation>Style [Guide d&apos;objet sélectionné]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="482"/>
        <source>Style [Aligned object guide]</source>
        <translation>Style [Guide d&apos;objet aligné]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="487"/>
        <source>Style [Mouse selection]</source>
        <translation>Style [Sélection de souris]</translation>
    </message>
    <message>
        <location filename="../settings.py" line="491"/>
        <source>Default paper width</source>
        <translation>Largeur du papier par défaut</translation>
    </message>
    <message>
        <location filename="../settings.py" line="494"/>
        <source>Default paper height</source>
        <translation>Hauteur du papier par défaut</translation>
    </message>
    <message>
        <location filename="../settings.py" line="497"/>
        <source>Display refresh interval</source>
        <translation>Intervalle de rafraichissement de l&apos;affichage</translation>
    </message>
    <message>
        <location filename="../settings.py" line="500"/>
        <source>Resizing area width</source>
        <translation>Largeur de la zone de redimensionnement</translation>
    </message>
    <message>
        <location filename="../settings.py" line="503"/>
        <source>Magnetic area width</source>
        <translation>Largeur de la zone aimantée</translation>
    </message>
    <message>
        <location filename="../settings.py" line="506"/>
        <source>Rotate precision (degrees)</source>
        <translation>Précision de la rotation (degrés)</translation>
    </message>
    <message>
        <location filename="../settings.py" line="509"/>
        <source>Mouse crop precision</source>
        <translation>Précision du rognage à la souris</translation>
    </message>
    <message>
        <location filename="../settings.py" line="194"/>
        <source>Indenting command</source>
        <translation>Commande d&apos;indentation</translation>
    </message>
</context>
<context>
    <name>Text</name>
    <message>
        <location filename="../element.py" line="1221"/>
        <location filename="../element.py" line="1242"/>
        <source>Text</source>
        <translation>Texte</translation>
    </message>
    <message>
        <location filename="../element.py" line="1224"/>
        <source>Align.</source>
        <translation>Align.</translation>
    </message>
    <message>
        <location filename="../element.py" line="1225"/>
        <source>Pages</source>
        <translation>Pages</translation>
    </message>
    <message>
        <location filename="../element.py" line="1226"/>
        <source>x guides</source>
        <translation>Guides x</translation>
    </message>
    <message>
        <location filename="../element.py" line="1227"/>
        <source>y guides</source>
        <translation>Guides y</translation>
    </message>
    <message>
        <location filename="../element.py" line="1220"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
</context>
<context>
    <name>TikzPicture</name>
    <message>
        <location filename="../element.py" line="2015"/>
        <source>Lock ratio</source>
        <translation>Bloquer le rapport</translation>
    </message>
    <message>
        <location filename="../element.py" line="2017"/>
        <source>Original ratio</source>
        <translation>Rapport de l&apos;original</translation>
    </message>
    <message>
        <location filename="../element.py" line="2019"/>
        <source>Angle</source>
        <translation>Angle</translation>
    </message>
    <message>
        <location filename="../element.py" line="2021"/>
        <source>Tikz command</source>
        <translation>Commande Tikz</translation>
    </message>
    <message>
        <location filename="../element.py" line="2023"/>
        <source>Pages</source>
        <translation>Pages</translation>
    </message>
    <message>
        <location filename="../element.py" line="2024"/>
        <source>x guides</source>
        <translation>Guides x</translation>
    </message>
    <message>
        <location filename="../element.py" line="2025"/>
        <source>y guides</source>
        <translation>Guides y</translation>
    </message>
    <message>
        <location filename="../element.py" line="2041"/>
        <source>Tikz Picture</source>
        <translation>Dessin Tikz</translation>
    </message>
</context>
</TS>
